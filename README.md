## Synopsis

This repository contains code that builds docker container with openstack clients and a wrapper to execute and use the clicode that builds docker container with openstack clients and a wrapper to execute and use the clients eliminating the need to install locally.



## **Pre-Requistes**
docker-engine 1.10+
** docker-compose

 - docker-compose is installed as a container from the ./scripts/build.sh which installs  docker-compose to /usr/local/bin/.  When running in a non *NIX envirionment docker-compose will need to be installed separately.


Example:

    yum install docker-engine

## Code Example

Spins up a docker container and installs the python openstack clients to interface with a running openstack cloud. To provide an interface for customers without the need to provide infra details.


## Motivation

Increase portability and flexibility on interfacing with Openstack clouds by removing the need to install client software on servers/vms.

### Installation

After cloning this repo updating key files are necessary before execution.

### Files

**./etc/openrc.sh**

The project builds a container with the python openstack CLI utilities, update this file with the correct credentials to use the cli.  A wrapper exists in ./scripts/openstack which copies the this file to the container, sources it and runs the commands.

### **Execution**
./build.sh

./openstack nova list
