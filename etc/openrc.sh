#!/usr/bin/env bash

#export OS_AUTH_URL=http://ccp1:5000/v2.0
## unsetting v3 items in case set
unset OS_PROJECT_ID
unset OS_PROJECT_NAME
unset OS_USER_DOMAIN_NAME
#
## In addition to the owning entity (tenant), OpenStack stores the entity
## performing the action as the **user**.
#export OS_USERNAME="jason.horn"
#
## With Keystone you pass the keystone password.
#echo "Please enter your OpenStack Password: "
#read -sr OS_PASSWORD_INPUT
#export OS_PASSWORD=$OS_PASSWORD_INPUT
#
## If your configuration has multiple regions, we set that information here.
## OS_REGION_NAME is optional and only valid in certain environments.
#export OS_REGION_NAME="RegionOne"
## Don't leave a blank variable, unset it if it was empty
#if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi
#
#
export OS_USERNAME=jason.horn
export OS_PASSWORD=
export OS_TENANT_NAME=DSR
export OS_AUTH_URL=http://ccp1.us.oracle.com:5000/v2.0
export OS_REGION_NAME=RegionOne
